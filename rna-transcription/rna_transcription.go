package strand

//ToRNA function returns a RNA compliment of a DNA strand
func ToRNA(dna string) string {

	result := ""

	if len(dna) == 0 {
		return result
	}

	for i := 0; i < len(dna); i++ {
		switch string(dna[i]) {
		case "G":
			result += "C"
		case "C":
			result += "G"
		case "T":
			result += "A"
		case "A":
			result += "U"
		default:
			result += ""
		}
	}
	return result
}
