//Package twofer means two for one, used to return one for you, one for me or one for name, one for me
package twofer

import "fmt"

//ShareWith function will return One for you, one for me when name is empty,
//otherwise it will returns One for $name, one for me, where $name is a variable
func ShareWith(name string) string {

	if len(name) == 0 {
		name = "you"
	}
	return fmt.Sprintf("One for %s, one for me.", name)

}
