//Package proverb generates relevent proverbs
package proverb

import "fmt"

const (
	stanza = "For want of a %s the %s was lost."
	last   = "And all for the want of a %s."
)

// Proverb function accepts array of string which consists of words and returns proverb array
func Proverb(rhyme []string) []string {

	var slice []string

	for i := 0; i < len(rhyme); i++ {

		if i == len(rhyme)-1 {
			slice = append(slice, fmt.Sprintf(last, rhyme[0]))
		} else {
			slice = append(slice, fmt.Sprintf(stanza, rhyme[i], rhyme[i+1]))
		}
	}
	return slice
}
