package accumulate

// Accumulate function changes a string according to an instruction
func Accumulate(s []string, fn func(string) string) []string {

	result := make([]string, len(s))

	for i, elem := range s {
		result[i] = fn(elem)
	}

	return result
}
