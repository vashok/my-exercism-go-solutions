//Package leap reports a year leap or not
package leap

// IsLeapYear function identify a year leap or not
func IsLeapYear(year int) bool {
	return year%4 == 0 && (year%100 != 0 || year%400 == 0)
}
