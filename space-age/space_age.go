//Package space calculate the age in different planets
package space

//Planet String consisit of different planet names
type Planet string

//Age accepts the term a man lived in a planet in seconds and the planet name, then it returns corresponding time period in earth year
func Age(seconds float64, planet Planet) float64 {

	var earthyear float64 = 31557600

	planets := map[Planet]float64{
		"Earth":   1,
		"Venus":   0.61519726,
		"Mercury": 0.2408467,
		"Mars":    1.8808158,
		"Jupiter": 11.862615,
		"Saturn":  29.447498,
		"Uranus":  84.016846,
		"Neptune": 164.79132,
	}
	return seconds / (planets[planet] * earthyear)
}
