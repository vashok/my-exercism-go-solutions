//Package raindrops converts a number to raindrop sound
package raindrops

import "strconv"

//Convert function accepts an integer and produces a string
func Convert(number int) string {

	sound := ""

	if number%3 == 0 {
		sound = "Pling"
	}
	if number%5 == 0 {
		sound += "Plang"
	}
	if number%7 == 0 {
		sound += "Plong"
	}
	if len(sound) == 0 {
		sound = strconv.Itoa(number)
	}

	return sound
}
