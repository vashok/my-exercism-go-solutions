//Package hamming is used to calculate hamming distance between two DNA strands
package hamming

import "errors"

// Distance function first return error if two dna strands are equal, otherwise it returns hamming distance
func Distance(a, b string) (int, error) {

	if len(a) != len(b) {
		return 0, errors.New("Lengths must be same")
	}
	distance := 0
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			distance++
		}
	}
	return distance, nil
}
