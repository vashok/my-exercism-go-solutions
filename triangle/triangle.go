//Package triangle accepts 3 numbers and return the type of triangle
package triangle

import "math"

// Kind represents type of triangule
type Kind string

const (
	//NaT is a constant of datatype Kind, represents Not a triangle
	NaT Kind = "not a triangle"
	//Equ is a constant of datatype Kind, represents Equilateral triangle
	Equ Kind = "equilateral"
	//Iso is a constant of datatype Kind, represents Isosceles triangle
	Iso Kind = "isosceles"
	//Sca is a constant of datatype Kind, represents scalene triangle
	Sca Kind = "scalene"
)

// KindFromSides returns Kind of triangle which is formed by sides of a,b,c
func KindFromSides(a, b, c float64) Kind {

	var k Kind

	if !isTriangle(a, b, c) {
		k = NaT
	} else if (a == b) && (b == c) {
		k = Equ
	} else if (a == b) || (b == c) || (a == c) {
		k = Iso
	} else {
		k = Sca
	}
	return k
}

//isSides determines whether a side is eligible or not
func isSides(s float64) bool {
	return s > 0 && !math.IsNaN(s) && !math.IsInf(s, 1)
}

//isTriangle is a function which determines whether the a,b,c forms a triangle or not
func isTriangle(a, b, c float64) bool {
	return isSides(a) && isSides(b) && isSides(c) && a+b >= c && b+c >= a && c+a >= b
}
